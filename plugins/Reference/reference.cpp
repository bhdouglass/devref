#include <QDebug>
#include <QStandardPaths>
#include <QFileSelector>

#include "reference.h"

Reference::Reference() {

}

QStringList Reference::standardPaths() const {
    QStringList paths;

    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::DesktopLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::DocumentsLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::FontsLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::FontsLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::ApplicationsLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::MusicLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::MusicLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::MoviesLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::PicturesLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::TempLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::HomeLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::DataLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::CacheLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::GenericCacheLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::GenericDataLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::RuntimeLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::ConfigLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::DownloadLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::GenericConfigLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::AppDataLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::AppLocalDataLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
    paths << QString("%1: %2").arg(QStandardPaths::displayName(QStandardPaths::AppConfigLocation)).arg(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));

    return paths;
}

QStringList Reference::fileSelectors() const {
    QFileSelector fileSelector;
    return fileSelector.allSelectors();
}

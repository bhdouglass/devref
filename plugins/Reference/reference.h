#ifndef REFERENCE_H
#define REFERENCE_H

#include <QObject>

class Reference: public QObject {
    Q_OBJECT

    Q_PROPERTY(QStringList standardPaths READ standardPaths CONSTANT)
    Q_PROPERTY(QStringList fileSelectors READ fileSelectors CONSTANT)

public:
    Reference();
    ~Reference() = default;

    QStringList standardPaths() const;
    QStringList fileSelectors() const;
};

#endif

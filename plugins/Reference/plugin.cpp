#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "reference.h"

void ReferencePlugin::registerTypes(const char *uri) {
    //@uri Reference
    qmlRegisterSingletonType<Reference>(uri, 1, 0, "Reference", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Reference; });
}

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

import Reference 1.0

Page {
    ListView {
        anchors.fill: parent

        model: Reference.standardPaths
        delegate: ListViewDelegateComponentNoIcon {
            showText: modelData
        }
    }
}

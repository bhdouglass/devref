import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

Page {
    ListView {
        anchors.fill: parent

        model: themeComponents
        delegate: Item {
            width: Math.min(parent.width - generalMargin * 2, units.gu(25))
            height: button.height + units.gu(3)
            anchors.horizontalCenter: parent.horizontalCenter

            Ubuntu.Button {
                id: button
                width: parent.width
                anchors.centerIn: parent
                text: modelData.name
                color: modelData.style
            }
        }
    }

    property var themeComponents: [
        { name: 'Normal Positive', style: theme.palette.normal.positive },
        { name: 'Disabled Positive', style: theme.palette.disabled.positive },
        { name: 'Focused Positive', style: theme.palette.focused.positive },
        { name: 'Highlighted Positive', style: theme.palette.highlighted.positive },
        { name: 'Selected Positive', style: theme.palette.selected.positive },
        { name: 'SelectedDisabled Positive', style: theme.palette.selectedDisabled.positive },

        { name: 'Normal Negative', style: theme.palette.normal.negative },
        { name: 'Disabled Negative', style: theme.palette.disabled.negative },
        { name: 'Focused Negative', style: theme.palette.focused.negative },
        { name: 'Highlighted Negative', style: theme.palette.highlighted.negative },
        { name: 'Selected Negative', style: theme.palette.selected.negative },
        { name: 'SelectedDisabled Negative', style: theme.palette.selectedDisabled.negative },

        { name: 'Normal Activity', style: theme.palette.normal.activity },
        { name: 'Disabled Activity', style: theme.palette.disabled.activity },
        { name: 'Focused Activity', style: theme.palette.focused.activity },
        { name: 'Highlighted Activity', style: theme.palette.highlighted.activity },
        { name: 'Selected Activity', style: theme.palette.selected.activity },
        { name: 'SelectedDisabled Activity', style: theme.palette.selectedDisabled.activity },

        { name: 'Normal Overlay', style: theme.palette.normal.overlay },
        { name: 'Disabled Overlay', style: theme.palette.disabled.overlay },
        { name: 'Focused Overlay', style: theme.palette.focused.overlay },
        { name: 'Highlighted Overlay', style: theme.palette.highlighted.overlay },
        { name: 'Selected Overlay', style: theme.palette.selected.overlay },
        { name: 'SelectedDisabled Overlay', style: theme.palette.selectedDisabled.overlay },

        { name: 'Normal Foreground', style: theme.palette.normal.foreground },
        { name: 'Disabled Foreground', style: theme.palette.disabled.foreground },
        { name: 'Focused Foreground', style: theme.palette.focused.foreground },
        { name: 'Highlighted Foreground', style: theme.palette.highlighted.foreground },
        { name: 'Selected Foreground', style: theme.palette.selected.foreground },
        { name: 'SelectedDisabled Foreground', style: theme.palette.selectedDisabled.foreground },
    ]
}

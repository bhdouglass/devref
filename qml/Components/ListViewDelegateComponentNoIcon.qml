import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu


ItemDelegate {
    //TODO: implement this?
    //property alias showDivider: ??.visible
    //Look into: https://github.com/ubports/qqc2-suru-style/blob/master/qqc2-suru/ItemDelegate.qml
    property alias showText: label.text

    width: parent.width
    height: listItemDefaultHeight

    Label {
        id: label
        verticalAlignment: Label.AlignVCenter

        anchors {
            fill: parent
            leftMargin: generalMargin
            rightMargin: generalMargin
        }
    }
}

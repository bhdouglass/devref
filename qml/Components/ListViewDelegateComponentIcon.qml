import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu


ItemDelegate {
    //TODO: implement this?
    //property alias showDivider: ??.visible
    //Look into: https://github.com/ubports/qqc2-suru-style/blob/master/qqc2-suru/ItemDelegate.qml
    property alias showText: label.text

    width: parent.width
    height: listItemDefaultHeight

    RowLayout {
        anchors {
            fill: parent
            leftMargin: generalMargin
            rightMargin: generalMargin
        }

        spacing: defaultSpacing

        Label {
            id: label
            Layout.fillWidth: true
            Layout.fillHeight: true

            verticalAlignment: Label.AlignVCenter
        }

        Ubuntu.Icon {
            Layout.fillHeight: true
            Layout.preferredWidth: height
            Layout.topMargin: units.gu(2)
            Layout.bottomMargin: units.gu(2)

            name: 'next'
        }
    }
}

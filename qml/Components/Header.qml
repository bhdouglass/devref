import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

ToolBar {
    property alias title: titleLabel.text
    property alias showBack: backButton.visible
    property alias showAbout: aboutButton.visible
    property alias showDivider: headerDivider.visible

    signal backButtonClicked()
    signal aboutButtonClicked()

    height: units.gu(6)

    property QtObject colors: QtObject {
        readonly property color divider: '#000000'
        readonly property color text: '#f5f5f5'
        readonly property color background: Ubuntu.UbuntuColors.orange
    }

    RowLayout {
        anchors {
            fill: parent
            leftMargin: backButton.visible ? units.gu(.5) : units.gu(1)
            rightMargin: units.gu(.5)
        }

        spacing: units.gu(.25)

        ToolButton {
            id: backButton

            Layout.fillHeight: true
            Layout.preferredWidth: height
            Layout.topMargin: units.gu(1.5)
            Layout.bottomMargin: units.gu(1.5)

            contentItem: Ubuntu.Icon {
                name: 'previous'
                color: colors.text
            }

            onClicked: backButtonClicked()

            // Don't show the pressed background
            background: Rectangle { visible: false }
        }

        Label {
            id: titleLabel
            Layout.fillWidth: true
            color: colors.text

            font.pixelSize: FontUtils.sizeToPixels('large')
            verticalAlignment: Qt.AlignVCenter
            wrapMode: Text.NoWrap
            elide: Text.ElideRight
        }

        ToolButton {
            id: aboutButton

            Layout.fillHeight: true
            Layout.preferredWidth: height
            Layout.topMargin: units.gu(1.5)
            Layout.bottomMargin: units.gu(1.5)

            contentItem: Ubuntu.Icon {
                name: 'info'
                color: colors.text
            }

            onClicked: aboutButtonClicked();

            // Don't show the pressed background
            background: Rectangle { visible: false }
        }
    }

    background: Rectangle {
        implicitHeight: units.gu(6)
        color: colors.background

        Rectangle {
            id: headerDivider
            height: units.dp(1)
            color: colors.divider

            anchors {
                left: parent.left
                right: parent.right
                top: parent.bottom
            }
        }
    }
}

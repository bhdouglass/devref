import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

Page {
    id: aboutPage
    objectName: 'aboutPage'
    title: i18n.tr('About')

    ListView {
        anchors.fill: parent

        clip: true

        header: ColumnLayout {
            id: column
            anchors {
                right: parent.right
                left: parent.left
            }

            spacing: defaultSpacing

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: ubuntuShapeIcon.height + generalMargin * 2

                Ubuntu.UbuntuShape {
                    id: ubuntuShapeIcon
                    anchors.centerIn: parent

                    width: units.gu(10)
                    height: units.gu(10)

                    image: Image {
                        source: Qt.resolvedUrl('../assets/logo.svg')

                        sourceSize {
                            width: ubuntuShapeIcon.width
                            height: ubuntuShapeIcon.height
                        }
                    }
                }
            }

            Label {
                Layout.fillWidth: true

                text: i18n.tr('Ubuntu Touch Dev Reference')
                horizontalAlignment: Label.AlignHCenter
                color: Ubuntu.UbuntuColors.orange
            }
        }

        model: [
            { text: 'Brian Douglass', url: 'https://bhdouglass.com' },
            { text: 'Joan CiberSheep', url: 'https://cibersheep.com' }
        ]

        delegate: ListViewDelegateComponentIcon {
            showText: modelData.text
            onClicked: Qt.openUrlExternally(modelData.url)
        }
    }
}

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

ApplicationWindow {
    id: root
    visible: true

    width: units.gu(45)
    height: units.gu(75)

    //Margins. We might want to change them in the future.
    //TODO: should they be readonly or just int?
    readonly property int generalMargin: units.gu(2)
    readonly property int listItemDefaultHeight: units.gu(6)
    readonly property int defaultSpacing: units.gu(1)

    header: Header {
        title: stackView.currentItem.title
        showBack: stackView.depth > 1
        showAbout: stackView.currentItem.objectName != 'aboutPage'
        showDivider: false

        onBackButtonClicked: stackView.pop()
        onAboutButtonClicked: stackView.push(Qt.resolvedUrl('AboutPage.qml'))
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: ListPage {}
    }
}

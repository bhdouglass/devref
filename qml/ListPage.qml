import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

Page {
    id: listPage
    title: i18n.tr('Dev Reference')

    property var pages: [
        {
            title: i18n.tr('Qt Standard Paths'),
            url: 'StandardPathsPage.qml',
        },
        {
            title: i18n.tr('Qt File Selectors'),
            url: 'FileSelectorsPage.qml',
        },
        {
            title: i18n.tr('Ubuntu Palette Theme Values'),
            url: 'ThemePalettePage.qml',
        },
        {
            title: i18n.tr('Suru Colors'),
            url: 'SuruColors.qml',
        }
    ]

    ListView {
        anchors.fill: parent

        model: pages
        delegate: ListViewDelegateComponentIcon {
            showText: modelData.title
            onClicked: stackView.push(Qt.resolvedUrl(modelData.url), {title: showText})
        }
    }
}

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3 as Ubuntu

import "Components"

Page {
    ListView {
        anchors.fill: parent

        model: themeComponents
        delegate: Item {
            width: Math.min(parent.width - generalMargin * 2, units.gu(25))
            height: button.height + units.gu(3)
            anchors.horizontalCenter: parent.horizontalCenter

            Ubuntu.Button {
                id: button
                width: parent.width
                anchors.centerIn: parent
                text: modelData.name
                color: modelData.color
            }
        }
    }

    property var themeComponents: [
        // TODO show light/dark theme
        // TODO highlight types
        // TODO text style/level

        { name: 'Highlight Color', color: Suru.highlightColor },
        { name: 'Active Focus Color', color: Suru.activeFocusColor },
        { name: 'Overlay Color', color: Suru.overlayColor },
        { name: 'Foreground Color', color: Suru.foregroundColor },
        { name: 'Secondary Foreground Color', color: Suru.secondaryForegroundColor },
        { name: 'Tertiary Foreground Color', color: Suru.tertiaryForegroundColor },
        { name: 'Neutral Color', color: Suru.neutralColor },
        { name: 'Background Color', color: Suru.backgroundColor },
        { name: 'Secondary Background Color', color: Suru.secondaryBackgroundColor },

        { name: 'Black', color: Suru.color(Suru.Black) },
        { name: 'Jet', color: Suru.color(Suru.Jet) },
        { name: 'Inkstone', color: Suru.color(Suru.Inkstone) },
        { name: 'Graphite', color: Suru.color(Suru.Graphite) },
        { name: 'Ash', color: Suru.color(Suru.Ash) },
        { name: 'Silk', color: Suru.color(Suru.Silk) },
        { name: 'Porcelain', color: Suru.color(Suru.Porcelain) },
        { name: 'White', color: Suru.color(Suru.White) },
        { name: 'Blue', color: Suru.color(Suru.Blue) },
        { name: 'Green', color: Suru.color(Suru.Green) },
        { name: 'Red', color: Suru.color(Suru.Red) },
        { name: 'Yellow', color: Suru.color(Suru.Yellow) },
        { name: 'Orange', color: Suru.color(Suru.Orange) },
        { name: 'Purple', color: Suru.color(Suru.Purple) },
        { name: 'Light Blue', color: Suru.color(Suru.LightBlue) },
        { name: 'Light Green', color: Suru.color(Suru.LightGreen) },
        { name: 'Light Yellow', color: Suru.color(Suru.LightYellow) },
        { name: 'Light Red', color: Suru.color(Suru.LightRed) },
    ]
}

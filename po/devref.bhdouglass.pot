# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the devref.bhdouglass package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: devref.bhdouglass\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-03 01:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AboutPage.qml:11
msgid "About"
msgstr ""

#: ../qml/AboutPage.qml:52 devref.desktop.in.h:1
msgid "Ubuntu Touch Dev Reference"
msgstr ""

#: ../qml/ListPage.qml:10
msgid "Dev Reference"
msgstr ""

#: ../qml/ListPage.qml:14
msgid "Qt Standard Paths"
msgstr ""

#: ../qml/ListPage.qml:18
msgid "Qt File Selectors"
msgstr ""

#: ../qml/ListPage.qml:22
msgid "Ubuntu Palette Theme Values"
msgstr ""

#: ../qml/ListPage.qml:26
msgid "Suru Colors"
msgstr ""
